package org.healifefit.web.calibase.exercises;

import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.healifefit.web.calibase.exercises.dto.ExerciseDto;
import org.healifefit.web.calibase.exercises.dto.ExerciseMapper;
import org.healifefit.web.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@GraphQLApi
public class ExerciseService {

    private ExerciseRepository exerciseRepository;

    public ExerciseService(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @GraphQLQuery
    public Page<ExerciseDto> exercises(int page) {
        var pageExercise = this.exerciseRepository.findAll(PageRequest.of(page, 10));
        return Utils.mapPageItems(pageExercise, ExerciseMapper.MAPPER::fromExercise);
    }

    @GraphQLMutation
    public ExerciseDto exercise(ExerciseDto exercise) {
        var newExercise = ExerciseMapper.MAPPER.toExercise(exercise);
        return ExerciseMapper.MAPPER.fromExercise(this.exerciseRepository.save(newExercise));
    }

    @GraphQLMutation
    public void deleteExercise(int id) {
        this.exerciseRepository.deleteById(id);
    }

}
