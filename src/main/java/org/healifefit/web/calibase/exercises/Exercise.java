package org.healifefit.web.calibase.exercises;

import lombok.Data;
import org.healifefit.web.calibase.muscles.Muscle;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @ManyToMany
    private List<Muscle> muscles;

}
