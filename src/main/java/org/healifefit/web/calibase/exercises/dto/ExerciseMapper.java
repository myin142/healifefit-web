package org.healifefit.web.calibase.exercises.dto;

import org.healifefit.web.calibase.exercises.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExerciseMapper {

    ExerciseMapper MAPPER = Mappers.getMapper(ExerciseMapper.class);

    Exercise toExercise(ExerciseDto exerciseDto);
    ExerciseDto fromExercise(Exercise exercise);
}
