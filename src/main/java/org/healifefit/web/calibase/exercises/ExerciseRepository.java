package org.healifefit.web.calibase.exercises;

import org.springframework.data.jpa.repository.JpaRepository;

interface ExerciseRepository extends JpaRepository<Exercise, Integer> {
}
