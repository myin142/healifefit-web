package org.healifefit.web.calibase.exercises.dto;

import lombok.Data;
import org.healifefit.web.calibase.muscles.dto.MuscleDto;

import java.util.List;

@Data
public class ExerciseDto {
    private Integer id;
    private String name;
    private List<MuscleDto> muscles;
}
