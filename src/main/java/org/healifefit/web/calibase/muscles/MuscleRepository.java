package org.healifefit.web.calibase.muscles;

import org.springframework.data.jpa.repository.JpaRepository;

interface MuscleRepository extends JpaRepository<Muscle, Integer> {
}
