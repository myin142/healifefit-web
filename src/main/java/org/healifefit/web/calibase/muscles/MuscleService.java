package org.healifefit.web.calibase.muscles;

import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.healifefit.web.calibase.muscles.dto.MuscleDto;
import org.healifefit.web.calibase.muscles.dto.MuscleMapper;
import org.healifefit.web.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@GraphQLApi
public class MuscleService {

    private MuscleRepository muscleRepository;

    public MuscleService(MuscleRepository muscleRepository) {
        this.muscleRepository = muscleRepository;
    }

    @GraphQLQuery
    public List<MuscleDto> muscles() {
        var muscleList = this.muscleRepository.findAll(); // Should not be too many
        return Utils.mapListItems(muscleList, MuscleMapper.MAPPER::fromMuscle);
    }
}
