package org.healifefit.web.calibase.muscles.dto;

import lombok.Data;

@Data
public class MuscleDto {
    private Integer id;
    private String name;
}
