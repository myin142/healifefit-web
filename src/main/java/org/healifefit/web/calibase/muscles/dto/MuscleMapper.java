package org.healifefit.web.calibase.muscles.dto;

import org.healifefit.web.calibase.muscles.Muscle;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MuscleMapper {

    MuscleMapper MAPPER = Mappers.getMapper(MuscleMapper.class);

    Muscle toMuscle(MuscleDto muscleDto);
    MuscleDto fromMuscle(Muscle muscle);
}
