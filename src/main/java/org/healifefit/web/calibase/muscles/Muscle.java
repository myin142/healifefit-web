package org.healifefit.web.calibase.muscles;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Muscle {

    @Id
    private int id;
    private String name;

}
