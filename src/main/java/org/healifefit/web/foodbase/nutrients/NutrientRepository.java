package org.healifefit.web.foodbase.nutrients;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NutrientRepository extends JpaRepository<Nutrient, Integer> {
}
