package org.healifefit.web.foodbase.nutrients;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Nutrient {

    @Id
    private int id;
    private String name;
    private String description;
    private double recommendedIntake;

}
