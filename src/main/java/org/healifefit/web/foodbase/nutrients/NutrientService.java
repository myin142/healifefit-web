package org.healifefit.web.foodbase.nutrients;

import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@GraphQLApi
public class NutrientService {

    private NutrientRepository nutrientRepository;

    public NutrientService(NutrientRepository nutrientRepository) {
        this.nutrientRepository = nutrientRepository;
    }

    @GraphQLQuery
    public Page<Nutrient> nutrients(int page) {
        return nutrientRepository.findAll(PageRequest.of(page, 10));
    }

}
