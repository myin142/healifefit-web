package org.healifefit.web.utils;

import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@UtilityClass
public class Utils {

    public <T, R> Page<R> mapPageItems(Page<T> page, Function<T, R> mapFn) {
        var newItems = page.get().map(mapFn).collect(Collectors.toList());
        return new PageImpl<>(newItems, page.getPageable(), page.getTotalElements());
    }

    public <T, R> List<R> mapListItems(List<T> list, Function<T, R> mapFn) {
        return list.stream().map(mapFn).collect(Collectors.toList());
    }

}
