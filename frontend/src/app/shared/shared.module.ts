import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatIconModule, MatInputModule } from '@angular/material';
import { InputFieldComponent } from './input-field/input-field.component';
import { ReactiveFormsModule } from '@angular/forms';

const modules = [
    CommonModule,
    MatButtonModule,
    MatIconModule,
];

const components = [
    InputFieldComponent,
];

@NgModule({
    imports: [
        ...modules,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
    ],
    exports: [
        ...modules,
        ...components,
    ],
    declarations: [
        ...components,
    ],
})
export class SharedModule {}
