import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldComponent } from './input-field.component';
import { MatInputModule } from '@angular/material';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { TestModule } from '../../test/test-module';
import { By } from '@angular/platform-browser';
import { TestUtils } from '../../test/test-utils';
import { DebugElement } from '@angular/core';

describe('InputFieldComponent', () => {
    let component: InputFieldComponent;
    let fixture: ComponentFixture<InputFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule(TestModule.extend({
            imports: [
                ReactiveFormsModule,
                MatInputModule,
            ],
            declarations: [InputFieldComponent],
        }));

        fixture = TestBed.createComponent(InputFieldComponent);
        component = fixture.componentInstance;
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should use value of control in input', () => {
        const control = new FormControl();
        control.setValue('Control');

        component.control = control;
        fixture.detectChanges();

        const input = getInputElement();
        expect(input.value).toEqual('Control');
    });

    it('should use input value in control', () => {
        const control = new FormControl();
        component.control = control;
        fixture.detectChanges();

        const input = getInputElement();
        TestUtils.changeInput(input, 'Input');

        expect(control.value).toEqual('Input');
    });

    it('should create label', () => {
        component.control = new FormControl();
        component.label = 'Label';
        fixture.detectChanges();

        const label = getLabelElement().nativeElement;
        expect(label.textContent).toContain('Label');
    });

    it('should not show label if not defined', () => {
        component.control = new FormControl();
        fixture.detectChanges();

        const label = getLabelElement();
        expect(label).toBeFalsy();
    });

    function getInputElement(): HTMLInputElement {
        return fixture.debugElement.query(By.css('input')).nativeElement;
    }

    function getLabelElement(): DebugElement {
        return fixture.debugElement.query(By.css('mat-label'));
    }
});
