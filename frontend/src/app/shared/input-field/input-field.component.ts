import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-input-field',
    templateUrl: './input-field.component.html',
    styleUrls: ['./input-field.component.css'],
})
export class InputFieldComponent implements OnInit {

    @Input() type = 'text';
    @Input() label: string;
    @Input() control: FormControl;

    constructor() {
    }

    ngOnInit() {
    }

}
