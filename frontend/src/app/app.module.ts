import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from './menu/menu.module';
import { SharedModule } from './shared/shared.module';
import { GraphQLModule } from './graphql.module';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        RouterModule.forRoot([
            {path: '', pathMatch: 'full', redirectTo: '/welcome'},
            {path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)},
            {path: 'food', loadChildren: () => import(`./pages/food-search/food-search.module`).then(m => m.FoodSearchModule)},
        ]),
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        SharedModule,
        MenuModule,
        GraphQLModule,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
