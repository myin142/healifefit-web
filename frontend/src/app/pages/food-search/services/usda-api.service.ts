import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchFoodRequest, SearchFoodResponse } from './food-search.model';
import { HttpClient } from '@angular/common/http';
import { NutrientRequest, NutrientResponse } from './nutrient.model';

@Injectable()
export class UsdaApiService {

    private static API_KEY = '8CDd1Sk5w1TAkwPKGlBTy8TSd1WRvQDpti0MGlxP';

    private static NEW_USDA_URL = `https://${UsdaApiService.API_KEY}@api.nal.usda.gov/fdc/v1`;
    private static OLD_USDA_URL = `https://api.nal.usda.gov/ndb`;

    private static SEARCH_URL = `${UsdaApiService.NEW_USDA_URL}/search`;
    private static LIST_URL = `${UsdaApiService.OLD_USDA_URL}/list`;

    public constructor(private http: HttpClient) {}

    public searchByName(request: SearchFoodRequest): Observable<SearchFoodResponse> {
        return this.httpCall(UsdaApiService.SEARCH_URL, request);
    }

    public nutrientList(request: NutrientRequest): Observable<NutrientResponse> {
        return this.httpCall(`${UsdaApiService.LIST_URL}?${this.toQueryParam(request)}`);
    }

    private toQueryParam(obj: object): string {
        return Object.keys(obj).map(key => `${key}=${obj[key]}`).join('&');
    }

    private httpCall<T>(url: string, body?: any): Observable<T> {
        if (body !== undefined) {
            return this.http.post<T>(url, body);
        }

        const urlWithApi = `${url}&api_key=${UsdaApiService.API_KEY}`;
        return this.http.get<T>(urlWithApi);
    }
}
