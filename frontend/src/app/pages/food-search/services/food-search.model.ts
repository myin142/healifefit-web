export interface SearchFoodRequest {
    generalSearchInput: string;
    pageNumber?: number;
    includeDataTypes?: { [type in DataType]: boolean };
    ingredients?: string;
    brandOwner?: string;
    requireAllWords?: boolean;
    sortField?: string;
    sortDirection?: SortDirection;
}

export interface SearchFoodResponse {
    foodSearchCriteria: SearchFoodRequest;
    totalHits: number;
    currentPage: number;
    totalPages: number;
    foods: SearchFood[];
}

export interface SearchFood {
    fdcId: number;
    description: string;
    scientificName: string;
    commonNames: string;
    additionalDescriptions: string;
    dataType: DataType;

    foodCode: string;
    gtinUpc: string;
    ndbNumber: number;
    publishedDate: Date;
    brandOwner: string;
    ingredients: string;
    allHighlightFields: string;
    score: number;
}

export enum SortDirection {
    ASC = 'asc',
    DESC = 'desc',
}

export enum DataType {
    SURVEY = 'Survey (FNDDS)',
    FOUNDATION = 'Foundation',
    BRANDED = 'Branded',
}
