export interface NutrientRequest {
    lt: ListType;
    max?: number;
    offset?: number;
    sort?: string;
}

export enum ListType {
    DERIVIATION_CODES = 'd',
    FOOD = 'f',
    FOOD_GROUP = 'g',
    ALL_NUTRIENTS = 'n',
    SPECIAL_NUTRIENTS = 'ns',
    STANDARD_NUTRIENTS = 'nr',
}

export interface NutrientResponse {
    start: number;
    end: number;
    total: number;
    sr: string;
    sort: string;
    item: Nutrient[];
}

export interface Nutrient {
    id: number;
    name: string;
}
