import { Component } from '@angular/core';
import { UsdaApiService } from './services/usda-api.service';
import { ListType } from './services/nutrient.model';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-food-search',
    templateUrl: './food-search.component.html',
})
export class FoodSearchComponent {

    searchControl = new FormControl();

    public constructor(private usdaApi: UsdaApiService) {
        usdaApi.nutrientList({
            lt: ListType.STANDARD_NUTRIENTS,
            max: 200,
        }).subscribe(response => {
            console.log(response);
        });
    }

}
