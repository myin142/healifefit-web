import { NgModule } from '@angular/core';
import { FoodSearchComponent } from './food-search.component';
import { RouterModule } from '@angular/router';
import { UsdaApiService } from './services/usda-api.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path: '', component: FoodSearchComponent},
        ]),
        SharedModule,
    ],
    declarations: [
        FoodSearchComponent,
    ],
    providers: [
        UsdaApiService,
    ],
})
export class FoodSearchModule {
}
