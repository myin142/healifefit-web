import { defaultTestModule, TestModule } from './test-module';
import { Injectable } from '@angular/core';

@Injectable()
class TestService {}

describe('TestModule', () => {

    it('should return default module', () => {
        expect(TestModule.extend({})).toEqual(defaultTestModule);
    });

    it('should add imports to module', () => {
        const module = TestModule.extend({
            imports: [TestService],
        });

        expect(module.imports).toContain(TestService);
    });

    it('should add providers to module', () => {
        const module = TestModule.extend({
            providers: [TestService],
        });

        expect(module.providers).toContain(TestService);
    });

    it('should add declarations to module', () => {
        const module = TestModule.extend({
            declarations: [TestService],
        });

        expect(module.declarations).toContain(TestService);
    });

});
