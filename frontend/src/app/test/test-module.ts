import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

export const defaultTestModule: NgModule = {
    imports: [
        NoopAnimationsModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
};

export class TestModule {
    public static extend(module: NgModule): NgModule {
        Object.keys(module).forEach(key => {
            if (!defaultTestModule[key]) {
                defaultTestModule[key] = [];
            }

            defaultTestModule[key] = [
                ...defaultTestModule[key],
                ...module[key],
            ];
        });

        return defaultTestModule;
    }
}
