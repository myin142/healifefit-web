export interface MenuLink {
    name: string;
    icon?: string;
    url?: string;
    items?: MenuLink[];
}
