import { NgModule } from '@angular/core';
import { MenuComponent } from './menu.component';
import { SharedModule } from '../shared/shared.module';
import { MatMenuModule, MatToolbarModule } from '@angular/material';

@NgModule({
    imports: [
        SharedModule,
        MatMenuModule,
        MatToolbarModule,
    ],
    declarations: [
        MenuComponent,
    ],
    exports: [
        MenuComponent,
    ],
})
export class MenuModule {}
