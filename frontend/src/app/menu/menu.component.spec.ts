import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuComponent } from './menu.component';
import { MatMenuModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { MenuLink } from './menu.model';
import { TestModule } from '../test/test-module';

describe('MenuComponent', () => {
    let component: MenuComponent;
    let fixture: ComponentFixture<MenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule(TestModule.extend({
            imports: [MatMenuModule],
            declarations: [MenuComponent],
        }));
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain a toolbar', () => {
        const toolbar = fixture.debugElement.query(By.css('mat-toolbar'));
        expect(toolbar).toBeTruthy();
    });

    it('should create menu buttons', () => {
        component.menuItem = [{name: ''}, {name: ''}];
        fixture.detectChanges();

        const buttons = fixture.debugElement.queryAll(By.css('button'));
        expect(buttons.length).toEqual(component.menuItem.length);
    });

    it('should open submenu on menu button click', () => {
        clickMenuWithSubmenu([]);

        const menuPanel = fixture.debugElement.query(By.css('.mat-menu-panel'));
        expect(menuPanel).toBeTruthy();
    });

    it('should open submenu with buttons', () => {
        clickMenuWithSubmenu([{name: ''}, {name: ''}]);

        const subMenuButtons = fixture.debugElement.queryAll(By.css('.mat-menu-panel button'));
        expect(subMenuButtons.length).toEqual(component.menuItem[0].items.length);
    });

    function clickMenuWithSubmenu(items: MenuLink[]) {
        component.menuItem = [
            {
                name: '',
                items: [...items],
            },
        ];
        fixture.detectChanges();
        fixture.debugElement.query(By.css('button')).nativeElement.click();
    }
});
