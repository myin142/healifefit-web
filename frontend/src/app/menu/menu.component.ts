import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { MenuLink } from './menu.model';

@Component({
    selector: 'app-menu',
    templateUrl: 'menu.component.html',
    styleUrls: ['menu.component.css'],
})
export class MenuComponent {
    @ContentChild(TemplateRef, {static: false}) linkTemplate: TemplateRef<any>;
    @Input() title: MenuLink;
    @Input() menuItem: MenuLink[];
}
