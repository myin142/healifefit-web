import { Component } from '@angular/core';
import { MenuLink } from './menu/menu.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title: MenuLink = {
        name: 'Healifefit',
        icon: 'home',
        url: '/',
    };

    links: MenuLink[] = [
        {
            name: 'Food',
            url: '/food',
        },
        {
            name: 'Training',
            url: '/training',
        },
        {
            name: 'Other',
            url: '/other',
        },
    ];

}
